# Bootloader for Cortex-M3 based microcontrollers


This is a bootloader project originaly built for the `STM32F103C8` microcontroller.
The code is divided into 2 main parts:

1- One on the uC side that sends commands and receives responses as well as the .hex file from the UI.

2- The other part is the UI on the PC side, it receives the commands and sends responses as well as the .hex for the uC.

The uC communicates with the UI via UART, and the UI part is written in VBscript,
also the UI code relies on the application [`Docklight Scripting`](https://docklight.de/).


# Available options:

- Boot to last software

- Program a new software

- Erase Flash memory


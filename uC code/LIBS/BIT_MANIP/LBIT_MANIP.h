/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LBIT_MANIP_H
#define LBIT_MANIP_H


/* brackets wrapping 'var', 'bitN' and 'val' in the macro expansion allow complex expressions
   without having to manually wrap them.
   ex: BIT_ASSIGN(ptr + 2, bit_num + 5, bit_val - 1);         // good
   vs. BIT_ASSIGN( (ptr + 2), (bit_num + 5), (bit_val - 1) ); // ugly
*/

#define BIT_SET(var, bitN)         ((var) |= (1 << (bitN)))
#define BIT_CLEAR(var, bitN)       ((var) &= ~(1 << (bitN)))
#define BIT_ASSIGN(var, bitN, val) (!!(val)) ? BIT_SET(var, bitN) : BIT_CLEAR(var, bitN)

#define BIT_TOGGLE(var, bitN)      ((var) ^= (1 << (bitN)))

#define BIT_GET(var, bitN)         (!!((var) & (1 << (bitN))))


#endif /* LBIT_MANIP_H */


/*
 MIT License

 Copyright (c) 2018 Mina Helmi

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// LIBS
#include "../STD_TYPES/LSTD_TYPES.h"
#include "../BIT_MANIP/LBIT_MANIP.h"
#include <stdio.h>
// own files
#include "LNUM_MANIP_private.h"
#include "LNUM_MANIP_interface.h"

u8 LNUM_strHexToU8(char* str)
{
    u8 buff = 0;
    sscanf(str, "%2lx", &buff);

    return buff;
}

u16 LNUM_strHexToU16(char* str)
{
    u16 buff = 0;
    sscanf(str, "%4lx", &buff);

    return buff;
}

u32 LNUM_strHexToU32(char* str)
{
    u32 buff = 0;
    sscanf(str, "%8lx", &buff);

    return buff;
}


u8 LNUM_strToU8(char* str)
{
    u8 buff = 0;
    sscanf(str, "%2hhu", &buff);

    return buff;
}

u16 LNUM_strToU16(char* str)
{
    u16 buff = 0;
    sscanf(str, "%4hu", &buff);

    return buff;
}

u32 LNUM_strToU32(char* str)
{
    u32 buff = 0;
    sscanf(str, "%8lu", &buff);

    return buff;
}




/*
 MIT License

 Copyright (c) 2018 Mina Helmi

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef MFPEC_INTERFACE_H_
#define MFPEC_INTERFACE_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
/*
typedef enum
{
    FPEC_OPT_bytes_reg1,
    FPEC_OPT_bytes_reg2,
    FPEC_OPT_bytes_reg3,
    FPEC_OPT_bytes_reg4,
} FPEC_OPT_bytes_reg_t;
*/
typedef enum
{
    FPEC_page_0  = 0x08000000,
    FPEC_page_1  = 0x08000400,
    FPEC_page_2  = 0x08000800,
    FPEC_page_3  = 0x08000C00,
    FPEC_page_4  = 0x08001000,
    FPEC_page_5  = 0x08001400,
    FPEC_page_6  = 0x08001800,
    FPEC_page_7  = 0x08001C00,
    FPEC_page_8  = 0x08002000,
    FPEC_page_9  = 0x08002400,
    FPEC_page_10 = 0x08002800,
    FPEC_page_11 = 0x08002C00,
    FPEC_page_12 = 0x08003000,
    FPEC_page_13 = 0x08003400,
    FPEC_page_14 = 0x08003800,
    FPEC_page_15 = 0x08003C00,
    FPEC_page_16 = 0x08004000,
    FPEC_page_17 = 0x08004400,
    FPEC_page_18 = 0x08004800,
    FPEC_page_19 = 0x08004C00,
    FPEC_page_20 = 0x08005000,
    FPEC_page_21 = 0x08005400,
    FPEC_page_22 = 0x08005800,
    FPEC_page_23 = 0x08005C00,
    FPEC_page_24 = 0x08006000,
    FPEC_page_25 = 0x08006400,
    FPEC_page_26 = 0x08006800,
    FPEC_page_27 = 0x08006C00,
    FPEC_page_28 = 0x08007000,
    FPEC_page_29 = 0x08007400,
    FPEC_page_30 = 0x08007800,
    FPEC_page_31 = 0x08007C00,
    FPEC_page_32 = 0x08008000,
    FPEC_page_33 = 0x08008400,
    FPEC_page_34 = 0x08008800,
    FPEC_page_35 = 0x08008C00,
    FPEC_page_36 = 0x08009000,
    FPEC_page_37 = 0x08009400,
    FPEC_page_38 = 0x08009800,
    FPEC_page_39 = 0x08009C00,
    FPEC_page_40 = 0x0800A000,
    FPEC_page_41 = 0x0800A400,
    FPEC_page_42 = 0x0800A800,
    FPEC_page_43 = 0x0800AC00,
    FPEC_page_44 = 0x0800B000,
    FPEC_page_45 = 0x0800B400,
    FPEC_page_46 = 0x0800B800,
    FPEC_page_47 = 0x0800BC00,
    FPEC_page_48 = 0x0800C000,
    FPEC_page_49 = 0x0800C400,
    FPEC_page_50 = 0x0800C800,
    FPEC_page_51 = 0x0800CC00,
    FPEC_page_52 = 0x0800D000,
    FPEC_page_53 = 0x0800D400,
    FPEC_page_54 = 0x0800D800,
    FPEC_page_55 = 0x0800DC00,
    FPEC_page_56 = 0x0800E000,
    FPEC_page_57 = 0x0800E400,
    FPEC_page_58 = 0x0800E800,
    FPEC_page_59 = 0x0800EC00,
    FPEC_page_60 = 0x0800F000,
    FPEC_page_61 = 0x0800F400,
    FPEC_page_62 = 0x0800F800,
    FPEC_page_63 = 0x0800FC00,
    FPEC_page_64 = 0x08010000,
} FPEC_page_t;

// --- flags --- //
u8 MFPEC_isEndOfOperation(void);
u8 MFPEC_isWriteProtectionError(void);
u8 MFPEC_isProgrammingError(void);
u8 MFPEC_isFlashOperationBusy(void);
// --------- //


// --- FPEC config --- //
u8 MFPEC_isFPEClocked(void);
void MFPEC_setFPEClock(u8 isOn);

u8 MFPEC_isPrefetchBufferEnabled(void);
void MFPEC_prefetchBufferEnable(u8 isOn);

u8 MFPEC_isHalfCycleAccessEnabled(void);
void MFPEC_halfCycleAccessEnable(u8 isOn);
// --------- //


// --- Flash mem. --- //
void MFPEC_flashPageErase(u32 addrInPage);
void MFPEC_flashMassErasePages(void);
void MFPEC_flashProgram(volatile u16* addr, const u16 data);
// --------- //

/*
// --- Option Bytes --- //
u8 MFPEC_isOPTbytesLocked(void);
void MFPEC_setOPTbytesLock(u8 isOn);
void MFPEC_OPTbytesErase(void);
void MFPEC_OPTbytesProgram(FPEC_OPT_bytes_reg_t optReg, u32 data);
// --------- //
*/

#endif /* MFPEC_INTERFACE_H_ */


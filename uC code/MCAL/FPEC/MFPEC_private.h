/*
 MIT License

 Copyright (c) 2018 Mina Helmi

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef MFPEC_PRIVATE_H_
#define MFPEC_PRIVATE_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef struct
{
    u32 FPEC_ACR;
    u32 FPEC_KEYR;
    u32 FPEC_OPTKEYR;
    u32 FPEC_SR;
    u32 FPEC_CR;
    u32 FPEC_AR;

    const u32 _PADDING_FPEC_AR;

    const u32 FPEC_OBR;
    const u32 FPEC_WRPR;

} FPEC_nv_t;

typedef volatile FPEC_nv_t* const FPEC_t;

typedef struct
{
    u16 RDP;
    u16 USER;
    u16 Data[2];
    u16 WRP[4];
} FPEC_Opt_bytes_nv_t;

typedef volatile FPEC_Opt_bytes_nv_t* const FPEC_Opt_bytes_t;

// FLASH_ACR
#define PRFTBS   5 // Prefetch buffer status
#define PRFTBE   4 // Prefetch buffer enable
#define HLFCYA   3 // Flash half cycle access enable
#define LATENCY2 2 // Latency {000: if 0 < SYSCLK <= 24 MHz, 001:if 24 MHz < SYSCLK <= 48 MHz, 010:if 48 MHz < SYSCLK <= 72 MHz}
#define LATENCY1 1
#define LATENCY0 0

// FLASH_SR
// *** all of these flags are reset by writing a 1 to them *** //
#define EOP      5 // end of operation, Set by hardware when a Flash operation (programming / erase) is completed
#define WRPRTERR 4 // write protection error, Set by hardware when programming a write-protected address of the Flash memory
#define PGERR    2 // Programming error Set by hardware when an address to be programmed contains a value different from '0xFFFF' before programming
#define BSY      0 // (READ ONLY) busy flag for flash operation (read, write, erase)

// FLASH_CR
#define OPTWRE 9 // Option bytes write enable, When set, the option bytes can be programmed, This bit can be reset by software
#define LOCK   7 // Write to 1 only. When it is set, it indicates that the FPEC and FLASH_CR are locked
#define STRT   6 // This bit triggers an ERASE operation when set,  set only by software and reset when the BSY bit is reset
#define OPTER  5 // Option byte erase
#define OPTPG  4 // Option byte programming
#define MER    2 // Mass erase
#define PER    1 // Page erase
#define PG     0 // Flash programming chosen

// FLASH_AR
// *** For Page Erase operations, this should be updated by software to indicate the chosen page

// FLASH_OBR
// *** Bit 1 RDPRT: Read protection, 1: Flash memory is read-protected, 0: Flash memory is NOT read-protected

// FLASH_WRPR
// *** This register contains the write-protection option bytes loaded by the OBL
// *** any bit: 1: Write protection not active, 0: Write protection active











#endif /* MFPEC_PRIVATE_H_ */


/*
 MIT License

 Copyright (c) 2018 Mina Helmi

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// LIBS
#include "../../LIBS/BIT_MANIP/LBIT_MANIP.h"
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
// own files
#include "MFPEC_interface.h"
#include "MFPEC_private.h"

// --- internal --- //
#define RDPRT_KEY   0xA5
#define KEY1        0x45670123
#define KEY2        0xCDEF89AB

static FPEC_t FPEC = (FPEC_t)0x40022000;
static FPEC_Opt_bytes_t Opt_bytes = (FPEC_Opt_bytes_t)0x1FFFF800;

static inline void MFPEC_clearEndOfOperation(void)
{
    BIT_SET(FPEC->FPEC_SR, EOP);
}

static inline void MFPEC_clearWriteProtectionError(void)
{
    BIT_SET(FPEC->FPEC_SR, WRPRTERR);
}

static inline void MFPEC_clearProgrammingError(void)
{
    BIT_SET(FPEC->FPEC_SR, PGERR);
}
// --------- //

// --- flags --- //
u8 MFPEC_isEndOfOperation(void)
{
   // Set by hardware when a Flash operation (programming / erase) is completed
   return BIT_GET(FPEC->FPEC_SR, EOP);
}

u8 MFPEC_isWriteProtectionError(void)
{
   // Set by hardware when programming a write-protected address of the Flash memory
   return BIT_GET(FPEC->FPEC_SR, WRPRTERR);
}

u8 MFPEC_isProgrammingError(void)
{
   // Set by hardware when an address to be programmed contains a value different from '0xFFFF' before programming
   return BIT_GET(FPEC->FPEC_SR, PGERR);
}

u8 MFPEC_isFlashOperationBusy(void)
{
    return BIT_GET(FPEC->FPEC_SR, BSY);
}
// --------- //


// --- FPEC config --- //
u8 MFPEC_isFPEClocked(void)
{
    return BIT_GET(FPEC->FPEC_CR, LOCK);
}

void MFPEC_setFPEClock(u8 isOn)
{
    if (isOn) // lock
    {
        BIT_SET(FPEC->FPEC_CR, LOCK);
    }
    else // unlock
    {
        FPEC->FPEC_KEYR = KEY1;
        FPEC->FPEC_KEYR = KEY2;
    }
}

u8 MFPEC_isPrefetchBufferEnabled(void)
{
    return BIT_GET(FPEC->FPEC_ACR, PRFTBS);
}

// TODO: The prefetch buffer must be kept on (FLASH_ACR[4]=�1�) when using a prescaler different from 1 on the AHB clock
void MFPEC_prefetchBufferEnable(u8 isOn)
{
    BIT_ASSIGN(FPEC->FPEC_ACR, PRFTBE, isOn);
}

u8 MFPEC_isHalfCycleAccessEnabled(void)
{
    return BIT_GET(FPEC->FPEC_ACR, HLFCYA);
}

void MFPEC_halfCycleAccessEnable(u8 isOn)
{
    BIT_ASSIGN(FPEC->FPEC_ACR, HLFCYA, isOn);
}
// --------- //


// --- Flash mem. --- //
void MFPEC_flashPageErase(u32 addrInPage)
{
    while (MFPEC_isFlashOperationBusy())
    {}

    // unlock FPEC
    if (MFPEC_isFPEClocked())
    {
        MFPEC_setFPEClock(0);
    }

    BIT_SET(FPEC->FPEC_CR, PER); // declare flash page erase mode

    FPEC->FPEC_AR = addrInPage; // addr MUST be within page range

    BIT_SET(FPEC->FPEC_CR, STRT); // trigger erase

    while (MFPEC_isFlashOperationBusy())
    {}

    BIT_CLEAR(FPEC->FPEC_CR, PER); // clear flash page erase mode

    MFPEC_clearEndOfOperation();

    // TODO: verify erased area
}

void MFPEC_flashMassErasePages(void)
{
    while (MFPEC_isFlashOperationBusy())
    {}

    // unlock FPEC
    if (MFPEC_isFPEClocked())
    {
        MFPEC_setFPEClock(0);
    }

    BIT_SET(FPEC->FPEC_CR, MER); // declare flash mass erase mode

    BIT_SET(FPEC->FPEC_CR, STRT); // trigger erase

    while (MFPEC_isFlashOperationBusy())
    {}

    BIT_CLEAR(FPEC->FPEC_CR, MER); // clear flash mass erase mode

    MFPEC_clearEndOfOperation();

    // TODO: verify erased area
}

void MFPEC_flashProgram(volatile u16* addr, const u16 data)
{
    while (MFPEC_isFlashOperationBusy())
    {}

    // unlock FPEC
    if (MFPEC_isFPEClocked())
    {
        MFPEC_setFPEClock(0);
    }

    BIT_SET(FPEC->FPEC_CR, PG); // declare programming mode

    // put data in flash addr
    FPEC->FPEC_AR = (u32)addr; // TODO: this is completely useless
    *addr = data;

    while (MFPEC_isFlashOperationBusy())
    {}

    BIT_CLEAR(FPEC->FPEC_CR, PG); // clear programming mode

    MFPEC_clearEndOfOperation();

    // TODO: verify programmed value
}
// --------- //

/*
// --- Option Bytes --- //
u8 MFPEC_isOPTbytesLocked(void)
{
    return !BIT_GET(FPEC->FPEC_CR, OPTWRE);
}

void MFPEC_setOPTbytesLock(u8 isOn)
{
    if (isOn) // lock
    {
        BIT_CLEAR(FPEC->FPEC_CR, OPTWRE);
    }
    else // unlock
    {
        FPEC->FPEC_OPTKEYR = KEY1;
        FPEC->FPEC_OPTKEYR = KEY2;
    }
}

void MFPEC_OPTbytesErase(void)
{
    while (MFPEC_isFlashOperationBusy())
    {}

    // unlock FPEC
    if (MFPEC_isFPEClocked())
    {
        MFPEC_setFPEClock(0);
    }

    // unlock Option bytes
    if (MFPEC_isOPTbytesLocked())
    {
        MFPEC_setOPTbytesLock(0);
    }

    BIT_SET(FPEC->FPEC_CR, OPTER); // declare Option bytes erase mode

    BIT_SET(FPEC->FPEC_CR, STRT); // trigger erase

    while (MFPEC_isFlashOperationBusy())
    {}

    BIT_CLEAR(FPEC->FPEC_CR, OPTER); // clear Option bytes erase mode

    MFPEC_clearEndOfOperation();

    // TODO: verify erased bytes
}

void MFPEC_OPTbytesProgram(FPEC_OPT_bytes_reg_t optReg, u32 data)
{
    while (MFPEC_isFlashOperationBusy())
    {}

    // unlock FPEC
    if (MFPEC_isFPEClocked())
    {
        MFPEC_setFPEClock(0);
    }

    // unlock Option bytes
    if (MFPEC_isOPTbytesLocked())
    {
        MFPEC_setOPTbytesLock(0);
    }

    BIT_SET(FPEC->FPEC_CR, OPTPG); // declare Option bytes programming mode

    // Write the data (half-word) to the desired address
    Opt_bytes->WRP[3] = 0xFF00;

    while (MFPEC_isFlashOperationBusy())
    {}

    BIT_CLEAR(FPEC->FPEC_CR, OPTPG); // clear Option bytes programming mode

    MFPEC_clearEndOfOperation();

    // TODO: verify programmed value
}
// --------- //
*/

/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MGPIO_INTERFACE_H_
#define MGPIO_INTERFACE_H_


// LIBS
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef enum
{
    GPIO_dir_input,
    GPIO_dir_outut_max_10MHz,
    GPIO_dir_outut_max_2MHz,
    GPIO_dir_outut_max_50MHz,
} GPIO_direction_t;

typedef enum
{
    GPIO_input_analog = 0,
    GPIO_input_floating,
    GPIO_input_pull_up_down,

    GPIO_output_push_pull = 0,
    GPIO_output_open_drain,
    GPIO_output_AF_push_pull,
    GPIO_output_AF_open_drain,
} GPIO_mode_t;

typedef enum
{
    IO_port_A,
    IO_port_B,
    IO_port_C,
} IO_port_t;

void MGPIO_setPinDirection(IO_port_t port_X, u8 pin_N, GPIO_direction_t dir, GPIO_mode_t mode);
void MGPIO_setPortDirection(IO_port_t port_X, GPIO_direction_t dir, GPIO_mode_t mode);

void MGPIO_setPinValue(IO_port_t port_X, u8 pin_N, u8 val);
void MGPIO_togglePinValue(IO_port_t port, u8 pin_N);

void MGPIO_setPortValue(IO_port_t port_X, u16 val);
void MGPIO_togglePortValue(IO_port_t port);

u8 MGPIO_getPinValue(IO_port_t port_X, u8 pin_N);
u16 MGPIO_getPortValue(IO_port_t port_X);

void MGPIO_activatePinPU(IO_port_t port_X, u8 pin_N);
void MGPIO_activatePortPU(IO_port_t port_X);

void MGPIO_activatePinPD(IO_port_t port_X, u8 pin_N);
void MGPIO_activatePortPD(IO_port_t port_X);


#endif /* MGPIO_INTERFACE_H_ */


/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MGPIO_PRIVATE_H_
#define MGPIO_PRIVATE_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef struct
{
    u32 GPIO_CRL;       // control reg (low)
    u32 GPIO_CRH;       // control reg (high)
    const u32 GPIO_IDR; // input data reg
    u32 GPIO_ODR;       // output data reg
    u32 GPIO_BSRR;      // bit set/reset
    u32 GPIO_BRR;      // bit reset
    u32 GPIO_LCKR;      // lock reg
} GPIO_port_nv_t;

typedef volatile GPIO_port_nv_t* GPIO_port_t;


#endif /* MGPIO_PRIVATE_H_ */


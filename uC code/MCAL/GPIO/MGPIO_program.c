/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "../../LIBS/BIT_MANIP/LBIT_MANIP.h"
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
// own files
#include "MGPIO_interface.h"
#include "MGPIO_private.h"

// --- internal --- //
static const GPIO_port_t GPIO_Port_A = (const GPIO_port_t)0x40010800;
static const GPIO_port_t GPIO_Port_B = (const GPIO_port_t)0x40010C00;
static const GPIO_port_t GPIO_Port_C = (const GPIO_port_t)0x40011000;
// --------- //

void MGPIO_setPinDirection(IO_port_t port, u8 pin_N, GPIO_direction_t dir, GPIO_mode_t mode)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return;
        break;
    }

    if (pin_N < 8) // use CRL
    {
        // dir
        pin_N *= 4; // get bit number (MODE<N>[0])
        BIT_ASSIGN(port_X->GPIO_CRL, pin_N,     BIT_GET(dir, 0));
        BIT_ASSIGN(port_X->GPIO_CRL, pin_N + 1, BIT_GET(dir, 1));

        // mode
        pin_N += 2; // get bit number (CNF<N>[0])
        BIT_ASSIGN(port_X->GPIO_CRL, pin_N,     BIT_GET(mode, 0));
        BIT_ASSIGN(port_X->GPIO_CRL, pin_N + 1, BIT_GET(mode, 1));
    }
    else // use CRH
    {
        // dir
        pin_N -= 8; // re-base pin number (8-15 ---> 0-7)
        pin_N *= 4; // get bit number (MODE<N>[0])
        BIT_ASSIGN(port_X->GPIO_CRH, pin_N,     BIT_GET(dir, 0));
        BIT_ASSIGN(port_X->GPIO_CRH, pin_N + 1, BIT_GET(dir, 1));

        // mode
        pin_N += 2; // get bit number (CNF<N>[0])
        BIT_ASSIGN(port_X->GPIO_CRH, pin_N,     BIT_GET(mode, 0));
        BIT_ASSIGN(port_X->GPIO_CRH, pin_N + 1, BIT_GET(mode, 1));
    }
}

void MGPIO_setPortDirection(IO_port_t port, GPIO_direction_t dir, GPIO_mode_t mode)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return;
        break;
    }

    for (u8 i = 8; i > 0; i--)
    {
        // dir
        BIT_ASSIGN(port_X->GPIO_CRL, 4*i,     BIT_GET(dir, 0));
        BIT_ASSIGN(port_X->GPIO_CRL, 4*i + 1, BIT_GET(dir, 1));

        BIT_ASSIGN(port_X->GPIO_CRH, 4*i,     BIT_GET(dir, 0));
        BIT_ASSIGN(port_X->GPIO_CRH, 4*i + 1, BIT_GET(dir, 1));

        // mode
        BIT_ASSIGN(port_X->GPIO_CRL, 4*i,     BIT_GET(mode, 0));
        BIT_ASSIGN(port_X->GPIO_CRL, 4*i + 1, BIT_GET(mode, 1));

        BIT_ASSIGN(port_X->GPIO_CRH, 4*i,     BIT_GET(mode, 0));
        BIT_ASSIGN(port_X->GPIO_CRH, 4*i + 1, BIT_GET(mode, 1));
    }
}

void MGPIO_setPinValue(IO_port_t port, u8 pin_N, u8 val)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return;
        break;
    }

    (!!val) ? (port_X->GPIO_BSRR = (1 << pin_N)) : (port_X->GPIO_BRR = (1 << pin_N));
}

void MGPIO_togglePinValue(IO_port_t port, u8 pin_N)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return;
        break;
    }

    BIT_GET(port_X->GPIO_ODR, pin_N) ? (port_X->GPIO_BRR = (1 << pin_N)) : (port_X->GPIO_BSRR = (1 << pin_N)) ;
}

void MGPIO_setPortValue(IO_port_t port, u16 val)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return;
        break;
    }

    port_X->GPIO_ODR = val;
}

void MGPIO_togglePortValue(IO_port_t port)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return;
        break;
    }

    port_X->GPIO_ODR = !(port_X->GPIO_ODR);
}

u8 MGPIO_getPinValue(IO_port_t port, u8 pin_N)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return 0;
        break;
    }

    return BIT_GET(port_X->GPIO_IDR, pin_N);
}

u16 MGPIO_getPortValue(IO_port_t port)
{
    GPIO_port_t port_X = 0;
    switch (port)
    {
        case IO_port_A:
            port_X = GPIO_Port_A;
        break;

        case IO_port_B:
            port_X = GPIO_Port_B;
        break;

        case IO_port_C:
            port_X = GPIO_Port_C;
        break;

        default:
            return 0;
        break;
    }

    return (u16)(port_X->GPIO_IDR);
}

void MGPIO_activatePinPU(IO_port_t port, u8 pin_N)
{
    MGPIO_setPinValue(port, pin_N, 1);
}

void MGPIO_activatePortPU(IO_port_t port)
{
    MGPIO_setPortValue(port, 0xFFFF);
}

void MGPIO_activatePinPD(IO_port_t port, u8 pin_N)
{
    MGPIO_setPinValue(port, pin_N, 0);
}

void MGPIO_activatePortPD(IO_port_t port)
{
    MGPIO_setPortValue(port, 0);
}


/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MRCC_INTERFACE_H_
#define MRCC_INTERFACE_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef enum
{
    HSE_source_2pin,
    HSE_source_1pin,
} HSE_source_t;

typedef enum
{
    PLL_mul_x2,
    PLL_mul_x3,
    PLL_mul_x4,
    PLL_mul_x5,
    PLL_mul_x6,
    PLL_mul_x7,
    PLL_mul_x8,
    PLL_mul_x9,
    PLL_mul_x10,
    PLL_mul_x11,
    PLL_mul_x12,
    PLL_mul_x13,
    PLL_mul_x14,
    PLL_mul_x15,
    PLL_mul_x16,
    PLL_mul_x16_2,
} PLL_mul_t;

typedef enum
{
    PLL_source_HSI_div2,
    PLL_source_HSE,
} PLL_source_t;

typedef enum
{
    SysClk_source_HSI,
    SysClk_source_HSE,
    SysClk_source_PLL,
} SysClk_source_t;

typedef enum
{
    MCO_source_no_clk,
    MCO_source_SysClk = 4,
    MCO_source_HSI,
    MCO_source_HSE,
    MCO_source_PLL_div2,
} MCO_source_t;

typedef enum
{
    USB_clk_PLL_div1_5,
    USB_clk_PLL_div1,
} USB_clk_t;

typedef enum
{
    ADC_clk_APB2_div2,
    ADC_clk_APB2_div4,
    ADC_clk_APB2_div6,
    ADC_clk_APB2_div8,
} ADC_clk_t;

typedef enum
{
    APB2_clk_AHB_div1,
    APB2_clk_AHB_div2 = 4,
    APB2_clk_AHB_div4,
    APB2_clk_AHB_div8,
    APB2_clk_AHB_div16,
} APB2_clk_t;

typedef enum
{
    APB1_clk_AHB_div1,
    APB1_clk_AHB_div2 = 4,
    APB1_clk_AHB_div4,
    APB1_clk_AHB_div8,
    APB1_clk_AHB_div16,
} APB1_clk_t;

typedef enum
{
    AHB_clk_SysClk_div1,
    AHB_clk_SysClk_div2 = 8,
    AHB_clk_SysClk_div4,
    AHB_clk_SysClk_div8,
    AHB_clk_SysClk_div16,
    AHB_clk_SysClk_div64,
    AHB_clk_SysClk_div128,
    AHB_clk_SysClk_div256,
    AHB_clk_SysClk_div512,
} AHB_clk_t;

typedef enum
{
    MRCC_port_A = 2,
    MRCC_port_B,
    MRCC_port_C,
} MRCC_port_t;

typedef enum
{
    MRCC_USART1 = 14,
    MRCC_USART2 = 17,
    MRCC_USART3,
    MRCC_USART4,
    MRCC_USART5,
} MRCC_USART_t;

typedef enum
{
    MRCC_ADC1 = 9,
    MRCC_ADC2 = 10,
    MRCC_ADC3 = 15,
} MRCC_ADC_t;

typedef enum
{
    MRCC_SPI1 = 12,
    MRCC_SPI2 = 14,
    MRCC_SPI3 = 15,
} MRCC_SPI_t;

typedef enum
{
    MRCC_I2C1 = 21,
    MRCC_I2C2 = 22,
} MRCC_I2C_t;

// --- HSI --- //
u8 MRCC_HSIisEnabled(void);
void MRCC_HSIenable(u8 isON);
// --------- //

// --- HSE --- //
u8 MRCC_HSEisEnabled(void);
void MRCC_HSEsetSource(HSE_source_t src);
HSE_source_t MRCC_HSEgetSource(void);
void MRCC_HSEenable(u8 isON);
void MRCC_HSEenableCSS(void);
// --------- //

// --- PLL --- //
u8 MRCC_PLLisEnabled(void);
void MRCC_PLLsetMul(PLL_mul_t mulFactor);
PLL_mul_t MRCC_PLLgetMul(void);
void MRCC_PLLdivHSEby2(u8 isDiv);
u8 MRCC_PLLisHSEdiv2(void);
void MRCC_PLLsetSource(PLL_source_t src);
PLL_source_t MRCC_PLLgetSource(void);
void MRCC_PLLenable(u8 isON);
// --------- //

// --- SysClock --- //
void MRCC_SysClkSetSource(SysClk_source_t src);
SysClk_source_t MRCC_SysClkGetSource(void);
// --------- //

// --- AHB --- //
void MRCC_AHBsetClk(AHB_clk_t clk);
AHB_clk_t MRCC_AHBgetClk(void);
// --------- //

// --- APB2 --- //
void MRCC_APB2setClk(APB2_clk_t clk);
// --------- //

// --- APB1 --- //
void MRCC_APB1setClk(APB1_clk_t clk);
// --------- //

// --- IO ports --- //
void MRCC_IOportEnableClk(MRCC_port_t port, u8 isOn);
void MRCC_AFIOenableClk(u8 isOn);
// --------- //

// --- ADCs --- //
void MRCC_ADCenableClk(MRCC_ADC_t adc, u8 isOn);
void MRCC_ADCsetClk(ADC_clk_t clk);
// --------- //

// --- comm. protocols --- //
void MRCC_USARTenableClk(MRCC_USART_t usart, u8 isOn);
void MRCC_SPIenableClk(MRCC_SPI_t spi, u8 isOn);
void MRCC_I2CenableClk(MRCC_I2C_t i2c, u8 isOn);
// --------- //

// --- flash interface --- //
void MRCC_FlashInterfaceEnableClk(u8 isOn);
// --------- //


#endif /* MRCC_INTERFACE_H_ */


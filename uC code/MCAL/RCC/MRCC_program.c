/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "../../LIBS/BIT_MANIP/LBIT_MANIP.h"
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
// own files
#include "MRCC_private.h"
#include "MRCC_interface.h"

// --- internal --- //
static RCC_t RCC = (RCC_t)0x40021000;

static inline u8 MRCC_HSIisReady(void)
{
    return BIT_GET(RCC->RCC_CR, HSIRDY);
}

static inline u8 MRCC_HSEisReady(void)
{
    return BIT_GET(RCC->RCC_CR, HSERDY);
}

static inline u8 MRCC_PLLisReady(void)
{
    return BIT_GET(RCC->RCC_CR, PLLRDY);
}
// --------- //

// --- HSI --- //
u8 MRCC_HSIisEnabled(void)
{
    return BIT_GET(RCC->RCC_CR, HSION);
}

void MRCC_HSIenable(u8 isON)
{
    if (isON) // turn on
    {
        BIT_SET(RCC->RCC_CR, HSION);
        while (!MRCC_HSIisReady())
        {}
    }
    else // turn off
    {
        BIT_CLEAR(RCC->RCC_CR, HSION);
        while (MRCC_HSIisReady())
        {}
    }
}
// --------- //

// --- HSE --- //
u8 MRCC_HSEisEnabled(void)
{
    return BIT_GET(RCC->RCC_CR, HSEON);
}

void MRCC_HSEsetSource(HSE_source_t src)
{
    if (MRCC_HSEisEnabled()) // if on
    {
        return;
    }

    BIT_ASSIGN(RCC->RCC_CR, HSEBYP, src);
}

HSE_source_t MRCC_HSEgetSource(void)
{
    return BIT_GET(RCC->RCC_CR, HSEBYP);
}

void MRCC_HSEenable(u8 isON)
{
    if (isON) // turn on
    {
        BIT_SET(RCC->RCC_CR, HSEON);
        while (!MRCC_HSEisReady())
        {}
    }
    else // turn off
    {
        BIT_CLEAR(RCC->RCC_CR, HSEON);
        while (MRCC_HSEisReady())
        {}
    }
}

void MRCC_HSEenableCSS(void)
{
    if (!MRCC_HSEisEnabled()) // if off
    {
        return;
    }

    BIT_SET(RCC->RCC_CR, CSSON);
}
// --------- //

// --- PLL --- //
u8 MRCC_PLLisEnabled(void)
{
    return BIT_GET(RCC->RCC_CR, PLLON);
}

void MRCC_PLLsetMul(PLL_mul_t mulFactor)
{
    if (MRCC_PLLisEnabled()) // if on
    {
        return;
    }

    BIT_ASSIGN(RCC->RCC_CFGR, PLLMUL3, BIT_GET(mulFactor, 3));
    BIT_ASSIGN(RCC->RCC_CFGR, PLLMUL2, BIT_GET(mulFactor, 2));
    BIT_ASSIGN(RCC->RCC_CFGR, PLLMUL1, BIT_GET(mulFactor, 1));
    BIT_ASSIGN(RCC->RCC_CFGR, PLLMUL0, BIT_GET(mulFactor, 0));
}

PLL_mul_t MRCC_PLLgetMul(void)
{
    return (
        (BIT_GET(RCC->RCC_CFGR, PLLMUL3) << 3)  |
        (BIT_GET(RCC->RCC_CFGR, PLLMUL2) << 2)  |
        (BIT_GET(RCC->RCC_CFGR, PLLMUL1) << 1)  |
         BIT_GET(RCC->RCC_CFGR, PLLMUL0)
           );
}

void MRCC_PLLdivHSEby2(u8 isDiv)
{
    if (MRCC_PLLisEnabled()) // if on
    {
        return;
    }

    if (isDiv) // div2
    {
        BIT_SET(RCC->RCC_CFGR, PLLXTPRE);
    }
    else // no div
    {
        BIT_CLEAR(RCC->RCC_CFGR, PLLXTPRE);
    }
}

u8 MRCC_PLLisHSEdiv2(void)
{
    return BIT_GET(RCC->RCC_CFGR, PLLXTPRE);
}

void MRCC_PLLsetSource(PLL_source_t src)
{
    if (MRCC_PLLisEnabled()) // if on
    {
        return;
    }

    BIT_ASSIGN(RCC->RCC_CFGR, PLLSRC, src);
}

PLL_source_t MRCC_PLLgetSource(void)
{
    return BIT_GET(RCC->RCC_CFGR, PLLSRC);
}

void MRCC_PLLenable(u8 isON)
{
    if (isON) // turn on
    {
        BIT_SET(RCC->RCC_CR, PLLON);
        while (!MRCC_PLLisReady())
        {}
    }
    else // turn off
    {
        BIT_CLEAR(RCC->RCC_CR, PLLON);
        while (MRCC_PLLisReady())
        {}
    }
}
// --------- //

// --- SysClock --- //
void MRCC_SysClkSetSource(SysClk_source_t src)
{
    BIT_ASSIGN(RCC->RCC_CFGR, SW1, BIT_GET(src, 1));
    BIT_ASSIGN(RCC->RCC_CFGR, SW0, BIT_GET(src, 0));
}

SysClk_source_t MRCC_SysClkGetSource(void)
{
    return (
        (BIT_GET(RCC->RCC_CFGR, SWS1) << 1) |
         BIT_GET(RCC->RCC_CFGR, SWS0)
           );
}
// --------- //

// --- AHB --- //
void MRCC_AHBsetClk(AHB_clk_t clk)
{
    BIT_ASSIGN(RCC->RCC_CFGR, HPRE3, BIT_GET(clk, 3));
    BIT_ASSIGN(RCC->RCC_CFGR, HPRE2, BIT_GET(clk, 2));
    BIT_ASSIGN(RCC->RCC_CFGR, HPRE1, BIT_GET(clk, 1));
    BIT_ASSIGN(RCC->RCC_CFGR, HPRE0, BIT_GET(clk, 0));
}

AHB_clk_t MRCC_AHBgetClk(void)
{
    return (
        (BIT_GET(RCC->RCC_CFGR, HPRE3) << 3) |
        (BIT_GET(RCC->RCC_CFGR, HPRE2) << 2) |
        (BIT_GET(RCC->RCC_CFGR, HPRE1) << 1) |
         BIT_GET(RCC->RCC_CFGR, HPRE0)
           );
}
// --------- //

// --- APB2 --- //
void MRCC_APB2setClk(APB2_clk_t clk)
{
    BIT_ASSIGN(RCC->RCC_CFGR, PPRE22, BIT_GET(clk, 2));
    BIT_ASSIGN(RCC->RCC_CFGR, PPRE21, BIT_GET(clk, 1));
    BIT_ASSIGN(RCC->RCC_CFGR, PPRE20, BIT_GET(clk, 0));
}
// --------- //

// --- APB1 --- //
void MRCC_APB1setClk(APB1_clk_t clk)
{
    BIT_ASSIGN(RCC->RCC_CFGR, PPRE12, BIT_GET(clk, 2));
    BIT_ASSIGN(RCC->RCC_CFGR, PPRE11, BIT_GET(clk, 1));
    BIT_ASSIGN(RCC->RCC_CFGR, PPRE10, BIT_GET(clk, 0));
}
// --------- //

// --- IO ports --- //
void MRCC_IOportEnableClk(MRCC_port_t port, u8 isOn)
{
    BIT_ASSIGN(RCC->RCC_APB2ENR, port, isOn);
}

void MRCC_AFIOenableClk(u8 isOn)
{
    BIT_ASSIGN(RCC->RCC_APB2ENR, AFIOEN, isOn);
}
// --------- //

// --- ADCs --- //
void MRCC_ADCenableClk(MRCC_ADC_t adc, u8 isOn)
{
    BIT_ASSIGN(RCC->RCC_APB2ENR, adc, isOn);
}

void MRCC_ADCsetClk(ADC_clk_t clk)
{
    BIT_ASSIGN( RCC->RCC_CFGR, ADCPRE1, BIT_GET(clk, 1) );
    BIT_ASSIGN( RCC->RCC_CFGR, ADCPRE0, BIT_GET(clk, 0) );
}
// --------- //

// --- comm. protocols --- //
void MRCC_USARTenableClk(MRCC_USART_t usart, u8 isOn)
{
    switch (usart)
    {
        case MRCC_USART1:
            BIT_ASSIGN(RCC->RCC_APB2ENR, usart, isOn);
        break;

        case MRCC_USART2:
        case MRCC_USART3:
        case MRCC_USART4:
        case MRCC_USART5:
            BIT_ASSIGN(RCC->RCC_APB1ENR, usart, isOn);
        break;

        default:
        break;
    }
}

void MRCC_SPIenableClk(MRCC_SPI_t spi, u8 isOn)
{
    switch (spi)
    {
        case MRCC_SPI1:
            BIT_ASSIGN(RCC->RCC_APB2ENR, spi, isOn);
        break;

        case MRCC_SPI2:
        case MRCC_SPI3:
            BIT_ASSIGN(RCC->RCC_APB1ENR, spi, isOn);
        break;

        default:
        break;
    }
}

void MRCC_I2CenableClk(MRCC_I2C_t i2c, u8 isOn)
{
    BIT_ASSIGN(RCC->RCC_APB1ENR, i2c, isOn);
}
// --------- //

// --- flash interface --- //
void MRCC_FlashInterfaceEnableClk(u8 isOn)
{
    BIT_ASSIGN(RCC->RCC_AHBENR, FLITFEN, isOn);
}
// --------- //


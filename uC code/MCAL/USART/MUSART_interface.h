/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MUSART_INTERFACE_H_
#define MUSART_INTERFACE_H_


// LIBS
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef enum
{
    USART_dev1,
    USART_dev2,
    USART_dev3,
} USART_dev_t;

typedef enum
{
    USART_data_size_8,
    USART_data_size_9,
} USART_data_size_t;

typedef enum
{
    USART_parity_disabled,
    USART_parity_enabled,
} USART_parity_state_t;

typedef enum
{
    USART_parity_type_even,
    USART_parity_type_odd,
    USART_parity_type_NONE,
} USART_parity_type_t;

typedef enum
{
    USART_stop_bits_1,
    USART_stop_bits_0_5,
    USART_stop_bits_2,
    USART_stop_bits_1_5,
} USART_stop_bits_t;

typedef void (*USART_TxC_CB)(void);
typedef void (*USART_RxC_CB)(u16);

// *** setup *** //
void MUSART_config(USART_dev_t usart_dev, u32 baud, u32 fclk, USART_data_size_t size,
                 USART_parity_state_t parityIsOn, USART_parity_type_t parity_type,
                 USART_stop_bits_t stop_bits, u8 isOn);
void MUSART_enableDev(USART_dev_t usart_dev, u8 isOn);
void MUSART_setBaud(USART_dev_t usart_dev, u32 baud, u32 fclk);
void MUSART_setDataSize(USART_dev_t usart_dev, USART_data_size_t size);
void MUSART_enableParity(USART_dev_t usart_dev, u8 isOn);
void MUSART_setParityType(USART_dev_t usart_dev, USART_parity_type_t parity_type);
void MUSART_setStopBits(USART_dev_t usart_dev, USART_stop_bits_t stop_bits);
void MUSART_enableTx(USART_dev_t usart_dev, u8 isOn);
void MUSART_enableRx(USART_dev_t usart_dev, u8 isOn);
// ********* //

// *** INTs *** //
void MUSART_registerTxC_CB(USART_TxC_CB ptr);
void MUSART_deregisterTxC_CB(void);

void MUSART_registerRxC_CB(USART_RxC_CB ptr);
void MUSART_deregisterRxC_CB(void);

void MUSART_enableTxCompleteINT(USART_dev_t usart_dev, u8 isOn);

void MUSART_enableRxCompleteINT(USART_dev_t usart_dev, u8 isOn);
// ********* //

// *** state/error check *** //
u8 MUSART_isIdleLineDetected(USART_dev_t usart_dev);
u8 MUSART_isOverrunError(USART_dev_t usart_dev);
u8 MUSART_isNoiseError(USART_dev_t usart_dev);
u8 MUSART_isFramingError(USART_dev_t usart_dev);
u8 MUSART_isParityError(USART_dev_t usart_dev);
// ********* //

// *** data receive/transmit *** //
void MUSART_receiveData(USART_dev_t usart_dev, u16 dst[], u16 len);

void MUSART_transmitData(USART_dev_t usart_dev, u16 src[], u16 len);
void MUSART_transmitChar(USART_dev_t usart_dev, char src);
void MUSART_transmitStr(USART_dev_t usart_dev, char* str);
void MUSART_transmitInt(USART_dev_t usart_dev, s32 num);
void MUSART_transmitFloat(USART_dev_t usart_dev, f32 floatNum, u8 accuracy);
// ********* //


#endif /* MUSART_INTERFACE_H_ */


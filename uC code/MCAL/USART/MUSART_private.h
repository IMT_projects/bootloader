/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MUSART_PRIVATE_H_
#define MUSART_PRIVATE_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef struct
{
    u32 USART_SR;
    u32 USART_DR;
    u32 USART_BRR;
    u32 USART_CR1;
    u32 USART_CR2;
    u32 USART_CR3;
    u32 USART_GTPR;
} USART_nv_t;

typedef volatile USART_nv_t* USART_t;

// USART_SR
#define TXE 7 // Transmit data register empty
#define TC 6 // Transmission complete
#define RXNE 5 // 1: Received data is ready to be read
// reset sequence for these flags: read the USART_SR register, then read the USART_DR register
#define IDLE 4 // is IDLE line detected
#define ORE 3 // overrun error
#define NE 2 // noise error
#define FE 1 // framing error
#define PE 0 // parity error,  The software must wait for the RXNE flag to be set before clearing the PE bit

// USART_BRR
// DIV_Mantissa [11:0]
// DIV_Fraction [3:0]

// USART_CR1
#define UE 13 // USART enable
#define M 12 // Word length
#define PCE 10 // Parity control enable, 1 enable, 0 disable
#define PS 9 // Parity selection
#define TXEIE 7 // TXE interrupt enable
#define TCIE 6 // Transmission complete interrupt enable
#define RXNEIE 5 // RXNE interrupt enable
#define TE 3 // Tx enable
#define RE 2 // Rx enable
#define SBK 0 // Send break: This bit set is used to send break characters. It can be set and cleared by software. It should be set by software, and will be reset by hardware during the stop bit of break.

// USART_CR2
#define STOP1 13 // STOP bits
#define STOP0 12
//
//// USART_GTPR
//#define PSC7 7 // Prescaler value
//#define PSC6 6
//#define PSC5 5
//#define PSC4 4
//#define PSC3 3
//#define PSC2 2
//#define PSC1 1
//#define PSC0 0


#endif /* MUSART_PRIVATE_H_ */


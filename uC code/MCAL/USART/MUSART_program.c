/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "../../LIBS/BIT_MANIP/LBIT_MANIP.h"
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
// own files
#include "MUSART_private.h"
#include "MUSART_interface.h"

// --- internal --- //
static const USART_t USART1 = (const USART_t)0x40013800;
static const USART_t USART2 = (const USART_t)0x40004400;
static const USART_t USART3 = (const USART_t)0x40004800;

static USART_TxC_CB USART_TxCompleteCB = 0;
static USART_RxC_CB USART_RxCompleteCB = 0;

// is buffer ready to accept data
__attribute((always_inline)) static inline u8 MUSART_isTxBufferReady(USART_t usart)
{
    return BIT_GET(usart->USART_SR, TXE);
}

// is data completely sent
__attribute((always_inline)) static inline u8 MUSART_isTransmissionComplete(USART_t usart)
{
    return BIT_GET(usart->USART_SR, TC);
}

__attribute((always_inline)) static inline void MUSART_clearIsTransmissionComplete(USART_t usart)
{
    BIT_CLEAR(usart->USART_SR, TC);
}

// is buffer ready to be read
__attribute((always_inline)) static inline u8 MUSART_isRxBufferReady(USART_t usart)
{
    return BIT_GET(usart->USART_SR, RXNE);
}

__attribute((always_inline)) static inline void MUSART_clearIsRxBufferReady(USART_t usart)
{
    BIT_CLEAR(usart->USART_SR, RXNE);
}
// --------- //

// *** setup *** //
void MUSART_config(USART_dev_t usart_dev, u32 baud, u32 fclk, USART_data_size_t size,
                 USART_parity_state_t parityIsOn, USART_parity_type_t parity_type,
                 USART_stop_bits_t stop_bits, u8 isOn)
{
    MUSART_setDataSize(usart_dev, size);
    MUSART_setStopBits(usart_dev, stop_bits);
    MUSART_setBaud(usart_dev, baud, fclk);
    MUSART_enableParity(usart_dev, parityIsOn);
    MUSART_setParityType(usart_dev, parity_type);
    MUSART_enableDev(usart_dev, isOn);
    MUSART_enableTx(usart_dev, 1);
    MUSART_enableRx(usart_dev, 1);

    // TODO: send IDLE frame
}

void MUSART_enableDev(USART_dev_t usart_dev, u8 isOn)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, UE, isOn);
}

void MUSART_setBaud(USART_dev_t usart_dev, u32 baud, u32 fclk)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    // source: http://libopencm3.org/docs/latest/stm32f1/html/usart__common__all_8c_source.html
    usart->USART_BRR = ((2 * fclk) + baud) / (2 * baud);
}

void MUSART_setDataSize(USART_dev_t usart_dev, USART_data_size_t size)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, M, size);
}

void MUSART_enableParity(USART_dev_t usart_dev, u8 isOn)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, PCE, isOn);
}

void MUSART_setParityType(USART_dev_t usart_dev, USART_parity_type_t parity_type)
{
    if (parity_type == USART_parity_type_NONE)
    {
        return;
    }

    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, PS, parity_type);
}

void MUSART_setStopBits(USART_dev_t usart_dev, USART_stop_bits_t stop_bits)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN( usart->USART_CR2, STOP1, BIT_GET(stop_bits, 1) );
    BIT_ASSIGN( usart->USART_CR2, STOP0, BIT_GET(stop_bits, 0) );
}

void MUSART_enableTx(USART_dev_t usart_dev, u8 isOn)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, TE, isOn);
}

void MUSART_enableRx(USART_dev_t usart_dev, u8 isOn)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, RE, isOn);
}
// ********* //

// *** INTs *** //
void MUSART_registerTxC_CB(USART_TxC_CB ptr)
{
    USART_TxCompleteCB = ptr;
}

void MUSART_deregisterTxC_CB(void)
{
    USART_TxCompleteCB = 0;
}

void MUSART_registerRxC_CB(USART_RxC_CB ptr)
{
    USART_RxCompleteCB = ptr;
}

void MUSART_deregisterRxC_CB(void)
{
    USART_RxCompleteCB = 0;
}

void MUSART_enableTxCompleteINT(USART_dev_t usart_dev, u8 isOn)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, TCIE, isOn);
}

void MUSART_enableRxCompleteINT(USART_dev_t usart_dev, u8 isOn)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    BIT_ASSIGN(usart->USART_CR1, RXNEIE, isOn);
}
// ********* //

// *** state/error check *** //
u8 MUSART_isIdleLineDetected(USART_dev_t usart_dev)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return 0;
        break;
    }

    return BIT_GET(usart->USART_SR, IDLE);
}

u8 MUSART_isOverrunError(USART_dev_t usart_dev)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return 0;
        break;
    }

    return BIT_GET(usart->USART_SR, ORE);
}

u8 MUSART_isNoiseError(USART_dev_t usart_dev)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return 0;
        break;
    }

    return BIT_GET(usart->USART_SR, NE);
}

u8 MUSART_isFramingError(USART_dev_t usart_dev)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return 0;
        break;
    }

    return BIT_GET(usart->USART_SR, FE);
}

u8 MUSART_isParityError(USART_dev_t usart_dev)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return 0;
        break;
    }

    return BIT_GET(usart->USART_SR, PE);
}
// ********* //

// *** data receive/transmit *** //
void MUSART_receiveData(USART_dev_t usart_dev, u16 dst[], u16 len)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    while (len--)
    {
        while (!MUSART_isRxBufferReady(usart))
        {}

        *dst++ = (u16)(usart->USART_DR);

        // The RXNE bit must be cleared before the end of the reception of
        // the next character, to avoid an overrun error.
        MUSART_clearIsRxBufferReady(usart);
    }
}

void MUSART_transmitData(USART_dev_t usart_dev, u16 src[], u16 len)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    while (len--)
    {
        while (!MUSART_isTxBufferReady(usart))
        {}

        usart->USART_DR = *src++;
    }

    while (!MUSART_isTransmissionComplete(usart))
    {}

    MUSART_clearIsTransmissionComplete(usart);
}

void MUSART_transmitChar(USART_dev_t usart_dev, char src)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    while (!MUSART_isTxBufferReady(usart))
    {}

    usart->USART_DR = src;

    while (!MUSART_isTransmissionComplete(usart))
    {}

    MUSART_clearIsTransmissionComplete(usart);
}

void MUSART_transmitStr(USART_dev_t usart_dev, char* str)
{
    USART_t usart;
    switch (usart_dev)
    {
        case USART_dev1:
            usart = USART1;
        break;

        case USART_dev2:
            usart = USART2;
        break;

        case USART_dev3:
            usart = USART3;
        break;

        default:
            return;
        break;
    }

    while (*str)
    {
        while (!MUSART_isTxBufferReady(usart))
        {}

        usart->USART_DR = *str;

        str++;
    }

    while (!MUSART_isTransmissionComplete(usart))
    {}

    MUSART_clearIsTransmissionComplete(usart);
}

void MUSART_transmitInt(USART_dev_t usart_dev, s32 num)
{
    u8 len = 0;

    if (num < 0)
    {
        num *= -1;
        MUSART_transmitChar(usart_dev, '-');
    }

    u16 digits_buffer[10]; // 2^32 = 4294967296 (10 digits max)

    // store in buffer
    do
    {
        digits_buffer[9 - len++] = (num % 10) + '0';
    }
    while (num /= 10);

    MUSART_transmitData(usart_dev, &digits_buffer[10 - len], len);
}

void MUSART_transmitFloat(USART_dev_t usart_dev, f32 floatNum, u8 accuracy)
{
    u8 isNegative = 0;

    if (floatNum < 0)
    {
        isNegative = 1;
        floatNum *= -1;
    }

    u32 int_part = (u32)floatNum; // int part
    floatNum -= int_part;         // float part

    u16 sign = '-';

    // this prevents writing '-' if no accuracy required and int_part = 0
    if (isNegative)
    {
        if (int_part || (accuracy && floatNum))
        {
            MUSART_transmitData(usart_dev, &sign, 1);
        }
    }

    MUSART_transmitInt(usart_dev, (s32)int_part);

    // return if no accuracy required or no floating part
    if (!accuracy || !floatNum)
        return;

    sign = '.';
    MUSART_transmitData(usart_dev, &sign, 1);

    // while there's is accuracy and floating part
    while (accuracy-- && floatNum)
    {
        floatNum *= 10; // move digit before floating point
        register const u8 current_digit = (u8)floatNum;
        MUSART_transmitChar(usart_dev, current_digit + '0');
        floatNum -= current_digit; // remove digit before floating point
    }

}
// ********* //


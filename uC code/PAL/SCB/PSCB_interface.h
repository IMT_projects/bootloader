/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef PSCB_INTERFACE_H_
#define PSCB_INTERFACE_H_


typedef enum
{
    Priority_group_7_4 = 3,
    Priority_group_7_5,
    Priority_group_7_6,
    Priority_group_7,
    Priority_group_none,
} Priority_group_t;

// --- PendSV --- //
void PSCB_setPendSVpriority(u8 priority);
void PSCB_setPendSVpending(u8 isPending);
u8 PSCB_isPendSVpending(void);
// --------- //

// --- SVCall --- //
void PSCB_setSVCallPriority(u8 priority);
// --------- //

// --- SysTick --- //
void PSCB_setSysTickPriority(u8 priority);
// --------- //

// --- INTs status --- //
u8 PSCB_isAnyINTPending(void);
u16 PSCB_getPendingINT(void);
u16 PSCB_getActiveINT(void);
u8 PSCB_isExceptionPreempted(void);
// --------- //

// --- MSP & PSP --- //
void PSCB_setMSP(u32 addr);
void PSCB_setPSP(u32 addr);
// --------- //

void PSCB_setPriorityGroupingBits(Priority_group_t grp_pr);

void PSCB_setVectorTableBaseAddr(u32 addr);

void PSCB_forceSoftwareReset(void);

void PSCB_sleepOnExceptionRet(u8 isSleep);


#endif /* PSCB_INTERFACE_H_ */


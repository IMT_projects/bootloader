/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef PSCB_PRIVATE_H_
#define PSCB_PRIVATE_H_


#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"

typedef struct
{
    const u32 SCB_CPUID;
    u32 SCB_ICSR;
    u32 SCB_VTOR;
    u32 SCB_AIRCR;
    u32 SCB_SCR;
    u32 SCB_CCR;
    u8 SCB_SHPR[3][4];
    u32 SCB_SHCRS;
    u32 SCB_CFSR;
    u32 SCB_HFSR;

    const u32 _PADDING_HFSR;

    u32 SCB_MMAR;
    u32 SCB_BFAR;
} SCB_nv_t;

typedef volatile SCB_nv_t* const SCB_t;

// SCB_ICSR
#define NMIPENDSET   31 // 1: NMI set-pending state, 0: none
#define PENDSVSET    28 // 1: PendSV set-pending state, 0: none
#define PENDSVCLR    27 // 1: PendSV clear-pending state, 0: none
#define PENDSTSET    26 // 1: SysTick set-pending state, 0: none
#define PENDSTCLR    25 // 1: SysTick clear-pending state, 0: none
#define ISRPENDING   22 // {r} 1: an INT is pending, 0: no INT is pending
#define VECTPENDING9 21 // who is pending
#define VECTPENDING8 20
#define VECTPENDING7 19
#define VECTPENDING6 18
#define VECTPENDING5 17
#define VECTPENDING4 16
#define VECTPENDING3 15
#define VECTPENDING2 14
#define VECTPENDING1 13
#define VECTPENDING0 12
#define RETOBASE     11 // 0: There are preempted active exceptions to execute, 1: There are no active exceptions, or the currently-executing exception is the only active exception
#define VECTACTIVE8  8  // who is currently active (0 thread mode)
#define VECTACTIVE7  7
#define VECTACTIVE6  6
#define VECTACTIVE5  5
#define VECTACTIVE4  4
#define VECTACTIVE3  3
#define VECTACTIVE2  2
#define VECTACTIVE1  1
#define VECTACTIVE0  0

// SCB_AIRCR
// 31-16 write 0x05FA when writing to the reg
#define PRIGROUP2   10 // Interrupt priority-grouping field
#define PRIGROUP1   9
#define PRIGROUP0   8
#define SYSRESETREQ 2 // System reset request, 1: Asserts a signal to the outer system

// SCB_SCR
#define SLEEPONEXIT 1 // 0: Do not sleep when returning to Thread mode, 1: Enter sleep or deep sleep on return from an interrupt service routine.


#endif /* PSCB_PRIVATE_H_ */


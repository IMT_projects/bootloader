/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "../../LIBS/BIT_MANIP/LBIT_MANIP.h"
#include "../../LIBS/STD_TYPES/LSTD_TYPES.h"
// own files
#include "PSCB_private.h"
#include "PSCB_interface.h"

// --- internal --- //
SCB_t SCB = (SCB_t)0xE000ED00;
// --------- //

// --- PendSV --- //
void PSCB_setPendSVpriority(u8 priority)
{
    SCB->SCB_SHPR[2][1] = priority << 4;
}

void PSCB_setPendSVpending(u8 isPending)
{
    (!!isPending)
            ? BIT_SET(SCB->SCB_ICSR, PENDSVSET)
            : BIT_SET(SCB->SCB_ICSR, PENDSVCLR);
}

u8 PSCB_isPendSVpending(void)
{
    return BIT_GET(SCB->SCB_ICSR, PENDSVSET);
}
// --------- //

// --- SVCall --- //
void PSCB_setSVCallPriority(u8 priority)
{
    SCB->SCB_SHPR[1][0] = priority << 4;
}
// --------- //

// --- SysTick --- //
void PSCB_setSysTickPriority(u8 priority)
{
    SCB->SCB_SHPR[2][0] = priority << 4;
}
// --------- //

// --- INTs status --- //
u8 PSCB_isAnyINTPending(void)
{
    return BIT_GET(SCB->SCB_ICSR, ISRPENDING);
}

u16 PSCB_getPendingINT(void)
{
    return ( (BIT_GET(SCB->SCB_ICSR, VECTPENDING9) << 9) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING8) << 8) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING7) << 7) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING6) << 6) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING5) << 5) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING4) << 4) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING3) << 3) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING2) << 2) |
             (BIT_GET(SCB->SCB_ICSR, VECTPENDING1) << 1) |
              BIT_GET(SCB->SCB_ICSR, VECTPENDING0) );
}

u16 PSCB_getActiveINT(void)
{
    return ( (BIT_GET(SCB->SCB_ICSR, VECTACTIVE8) << 8) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE7) << 7) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE6) << 6) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE5) << 5) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE4) << 4) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE3) << 3) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE2) << 2) |
             (BIT_GET(SCB->SCB_ICSR, VECTACTIVE1) << 1) |
              BIT_GET(SCB->SCB_ICSR, VECTACTIVE0) );
}

u8 PSCB_isExceptionPreempted(void)
{
    return !BIT_GET(SCB->SCB_ICSR, RETOBASE);
}
// --------- //

// --- MSP & PSP --- //
void PSCB_setMSP(u32 addr)
{
    register const u32 addr_ = addr;

    asm volatile(
            "msr msp, %0 \t\n" // MSP = addr
            :           // no output
            :"r"(addr_) // input is register represented as addr_
            );
}

void PSCB_setPSP(u32 addr)
{
    register const u32 addr_ = addr;

    asm volatile(
            "msr psp, %0 \t\n" // PSP = addr
            :           // no output
            :"r"(addr_) // input is register represented as addr_
            );
}
// --------- //

void PSCB_setPriorityGroupingBits(Priority_group_t grp_pr)
{
    volatile u32 scb_aircr_ = SCB->SCB_AIRCR;

    scb_aircr_ &= 0x0000FFFF;
    scb_aircr_ |= 0x05FA0000;
    BIT_ASSIGN(scb_aircr_, PRIGROUP2, BIT_GET(grp_pr, 2));
    BIT_ASSIGN(scb_aircr_, PRIGROUP1, BIT_GET(grp_pr, 1));
    BIT_ASSIGN(scb_aircr_, PRIGROUP0, BIT_GET(grp_pr, 0));

    SCB->SCB_AIRCR = scb_aircr_;
}

void PSCB_setVectorTableBaseAddr(u32 addr)
{
    SCB->SCB_VTOR = addr;
}

void PSCB_forceSoftwareReset(void)
{
    volatile u32 scb_aircr_ = SCB->SCB_AIRCR;

    scb_aircr_ &= 0x0000FFFF;
    scb_aircr_ |= 0x05FA0000;
    BIT_SET(scb_aircr_, SYSRESETREQ);

    SCB->SCB_AIRCR = scb_aircr_;
}

void PSCB_sleepOnExceptionRet(u8 isSleep)
{
    BIT_ASSIGN(SCB->SCB_SCR, SLEEPONEXIT, isSleep);
}


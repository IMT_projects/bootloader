/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "../../../LIBS/STD_TYPES/LSTD_TYPES.h"
// MCAL
#include "../../../MCAL/RCC/MRCC_interface.h"
#include "../../../MCAL/GPIO/MGPIO_interface.h"
#include "../../../MCAL/USART/MUSART_interface.h"
// own files
#include "SUSART_interface.h"
#include "SUSART_private.h"

// --- internal --- //
#define PIN_TX1 9
#define PIN_RX1 10

#define PIN_TX2 2
#define PIN_RX2 3

#define PIN_TX3 10
#define PIN_RX3 11
// --------- //

void SUSART_init(USART_dev_t usart_dev, u32 baud, u32 fclk, USART_data_size_t size,
                 USART_parity_state_t parityIsOn, USART_parity_type_t parity_type, USART_stop_bits_t stop_bits)
{
    MRCC_AFIOenableClk(1); // NOT necessary

    switch (usart_dev)
    {
        case USART_dev1:
            // enable port clock
            MRCC_IOportEnableClk(MRCC_port_A, 1);

            // enable USART clock
            MRCC_USARTenableClk(MRCC_USART1, 1);

            // config pin Tx
            MGPIO_setPinDirection(IO_port_A, PIN_TX1, GPIO_dir_outut_max_50MHz, GPIO_output_AF_push_pull);

            // config pin Rx
            MGPIO_setPinDirection(IO_port_A, PIN_RX1, GPIO_dir_input, GPIO_input_floating);
        break;

        case USART_dev2:
            // enable port clock
            MRCC_IOportEnableClk(MRCC_port_A, 1);

            // enable USART clock
            MRCC_USARTenableClk(MRCC_USART2, 1);

            // config pin Tx
            MGPIO_setPinDirection(IO_port_A, PIN_TX2, GPIO_dir_outut_max_50MHz, GPIO_output_AF_push_pull);

            // config pin Rx
            MGPIO_setPinDirection(IO_port_A, PIN_RX2, GPIO_dir_input, GPIO_input_floating);
        break;

        case USART_dev3:
            // enable port clock
            MRCC_IOportEnableClk(MRCC_port_B, 1);

            // enable USART clock
            MRCC_USARTenableClk(MRCC_USART3, 1);

            // config pin Tx
            MGPIO_setPinDirection(IO_port_B, PIN_TX3, GPIO_dir_outut_max_50MHz, GPIO_output_AF_push_pull);

            // config pin Rx
            MGPIO_setPinDirection(IO_port_B, PIN_RX3, GPIO_dir_input, GPIO_input_floating);
        break;

        default:
            return;
        break;
    }

    MUSART_config(usart_dev, baud, fclk, size, parityIsOn, parity_type, stop_bits, 1);
}


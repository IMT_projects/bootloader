/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// LIBS
#include "LIBS/STD_TYPES/LSTD_TYPES.h"
#include "LIBS/NUM_MANIP/LNUM_MANIP_interface.h"
// PAL
#include "PAL/SCB/PSCB_interface.h"
// MCAL
#include "MCAL/FPEC/MFPEC_interface.h"
#include "MCAL/RCC/MRCC_interface.h"
#include "MCAL/USART/MUSART_interface.h"
#include "MCAL/GPIO/MGPIO_interface.h"
// HAL
// Services
#include "Services/MCAL/USART/SUSART_interface.h"

// definitions
#define BOOTLOADER_OFFSET    (0x2000)              // in bytes, this is the offset from page_0
#define BOOTLOADER_PAGE_SIZE 1024
#define CC_LEN               2
#define ADDR_LEN             4
#define RT_LEN               2
#define DATA_LEN             32
#define CHECKSUM_LEN         2
#define RX_BUFF_LEN          (DATA_LEN + ADDR_LEN) // the 2 biggest fields should be sufficient

// custom types
typedef enum
{
    Record_type_data      = 0,
    Record_type_EOF       = 1,
    Record_type_base      = 4,
    Record_type_last_addr = 5,
} Record_type_t;

typedef enum
{
    User_mode_RunSW,
    User_mode_FlashProg,
    User_mode_FlashErase,

    User_mode_UNDEFINED,
} User_mode_t;

typedef enum
{
    Tx_inform_UI_RunSW       = '!',
    Tx_inform_UI_FlashProg   = '@',
    Tx_inform_UI_FlashErase  = '#',
    Tx_inform_UI_RecordHalf1 = '$',
    Tx_inform_UI_RecordHalf2 = '%',
    Tx_inform_UI_EndComm     = '~',
} Tx_inform_UI_t;

typedef enum
{
    UI_inform_Rx_CommStart          = '^',
    UI_inform_Rx_HEXfileStatusOK    = '&',
    UI_inform_Rx_HEXfileStatusError = '*',
} UI_inform_Rx_t;

typedef struct
{
    u8 bytesCount;
    u16 addr;
    Record_type_t recordType;
    u8 data[DATA_LEN];
    u8 checksum;
} Record_t;

typedef void (*Function_t)(void);

// global objects
Function_t addr_to_call = 0;

// functions prototypes
u8 APP_promptMode(void);
void APP_modeProgramFlash(void);
void APP_modeRunLastSW(void);
void APP_modeEraseFlash(void);


int main(void)
{
    // reset clock source to HSI
    // Note: HSI must be enabled for FPEC to work
    MRCC_HSIenable(1);
    MRCC_SysClkSetSource(SysClk_source_HSI);

    // close other clock sources (safety procedure to allow further modification)
    MRCC_PLLenable(0);
    MRCC_HSEenable(0);

    // config and turn on PLL
    MRCC_PLLsetSource(PLL_source_HSI_div2); // PLL_clk = 4MHz
    MRCC_PLLsetMul(PLL_mul_x16);            // PLL_clk = 64 MHz
    MRCC_PLLenable(1);

    // set new SysClk as PLL
    MRCC_SysClkSetSource(SysClk_source_PLL); // SysClk = 64MHz

    // set buses prescalers
    MRCC_AHBsetClk(AHB_clk_SysClk_div1); // AHB_clk  = 64 MHz
    MRCC_APB2setClk(APB2_clk_AHB_div2);  // APB2_clk = 32 MHz
    MRCC_APB1setClk(APB1_clk_AHB_div2);  // APB1_clk = 32 MHz

    // enable clock for flash interface
    MRCC_FlashInterfaceEnableClk(1);

    // unlock FPEC
    MFPEC_setFPEClock(0);

    // init UART
    SUSART_init(
                USART_dev1,             // use USART device #1
                230400,                 // baud rate
                32000000,               // clock of the bus feeding the USART device
                USART_data_size_8,      // data bits in frame = 8
                USART_parity_disabled,  // no parity
                USART_parity_type_NONE, // parity type NONE
                USART_stop_bits_1       // stop bits in frame = 1
                );

    u16 choice = 0;  // catches various inputs
    u8 isRepeat = 0; // do we want to repeat the choice menu

    // wait for bootloader UI to init comm
    do
    {
        MUSART_receiveData(USART_dev1, &choice, 1);
    } while (choice != UI_inform_Rx_CommStart);

    // get mode {run SW, program flash, etc...}
    do
    {
        isRepeat = 0;

        switch (APP_promptMode())
        {
            case User_mode_RunSW:
                MUSART_transmitChar(USART_dev1, Tx_inform_UI_RunSW);       // inform the UI of the choice
                APP_modeRunLastSW();
                MUSART_transmitChar(USART_dev1, Tx_inform_UI_EndComm);     // end the communication
            break;

            case User_mode_FlashProg:
                MUSART_transmitChar(USART_dev1, Tx_inform_UI_FlashProg);   // inform the UI of the choice

                MUSART_receiveData(USART_dev1, &choice, 1);                // wait for the UI response
                if (choice == UI_inform_Rx_HEXfileStatusOK)                // file is opened successfully
                {
                    APP_modeProgramFlash();
                    MUSART_transmitChar(USART_dev1, Tx_inform_UI_EndComm); // end the communication
                }
                else // file is not opened (operation cancelled by user)
                {
                    isRepeat = 1;
                }
            break;

            case User_mode_FlashErase:
                isRepeat = 1;
                MUSART_transmitChar(USART_dev1, Tx_inform_UI_FlashErase);  // inform the UI of the choice
                APP_modeEraseFlash();
            break;

            default:
                // ERROR
                isRepeat = 1;
            break;
        }
    } while (isRepeat);

    // *** cleanup after USART1 *** //
    // config pin Rx
    MGPIO_setPinDirection(IO_port_A, 10, GPIO_dir_input, GPIO_input_floating);

    // config pin Tx
    MGPIO_setPinDirection(IO_port_A, 9, GPIO_dir_input, GPIO_input_floating);

    // disable USART
    MUSART_enableDev(USART_dev1, 0);

    // disable USART clock
    MRCC_USARTenableClk(MRCC_USART1, 0);

    // disable port clock
    MRCC_IOportEnableClk(MRCC_port_A, 0);

    // disable AFIO clock
    MRCC_AFIOenableClk(0);

    // *** cleanup after FPEC *** //
    // lock FPEC
    MFPEC_setFPEClock(1);

    // disable clock for flash interface
    MRCC_FlashInterfaceEnableClk(0);

    // restore SysClk
    MRCC_SysClkSetSource(SysClk_source_HSI);

    // turn off and reset PLL
    MRCC_PLLenable(0);
    MRCC_PLLsetSource(PLL_source_HSI_div2);
    MRCC_PLLsetMul(PLL_mul_x2);

    // reset buses prescalers
    MRCC_AHBsetClk(AHB_clk_SysClk_div1); // AHB_clk  = 8 MHz
    MRCC_APB2setClk(APB2_clk_AHB_div1);  // APB2_clk = 8 MHz
    MRCC_APB1setClk(APB1_clk_AHB_div1);  // APB1_clk = 8 MHz

    if ( ((u32)addr_to_call >= (u32)FPEC_page_0) &&
         ((u32)addr_to_call < (u32)FPEC_page_64) ) // addr within range
    {
        PSCB_setVectorTableBaseAddr(FPEC_page_0 + BOOTLOADER_OFFSET);
        PSCB_setMSP( *(u32*)(FPEC_page_0 + BOOTLOADER_OFFSET) );
        addr_to_call();
    }
    else // addr out of range
    {
        // ERROR
    }

    // we shouldn't get here
    while (1)
    {

    }

    return 0;
}

User_mode_t APP_promptMode(void)
{
    u16 choice = 0;

    // prompt the user for an option
    MUSART_transmitStr(USART_dev1, "[R]un software\r\n");
    MUSART_transmitStr(USART_dev1, "[P]rogram flash\r\n");
    MUSART_transmitStr(USART_dev1, "[E]rase flash\r\n");
    MUSART_transmitStr(USART_dev1, ">> _");

    // trap until a valid choice is entered
    while ((choice != 'r') && (choice != 'R') &&
           (choice != 'p') && (choice != 'P') &&
           (choice != 'e') && (choice != 'E'))
    {
        MUSART_receiveData(USART_dev1, &choice, 1);
    }

    if ((choice == 'r') || (choice == 'R'))
    {
        return User_mode_RunSW;
    }
    else if ((choice == 'p') || (choice == 'P'))
    {
        return User_mode_FlashProg;
    }
    else if ((choice == 'e') || (choice == 'E'))
    {
        return User_mode_FlashErase;
    }
    else
    {
        return User_mode_UNDEFINED;
    }
}

void APP_modeProgramFlash(void)
{
    Record_t current_record     = {0}; // always contains the current record
    u16 rx_buff[RX_BUFF_LEN]    = {0}; // USART receiver
    char input[RX_BUFF_LEN - 1] = {0}; // used to convert the array 'rx_buff' to u8[], -1 because we're not interested in the colon
    u8 isEOF                    = 0;   // is end of file reached
    u32 base_addr               = 0;   // contains the last base address (RT = 4)
    u32 current_addr            = 0;   // contains the absolute mem. addr of the current record
    u32 last_addr               = 0;   // last addr to be used by the current record
    u8 i                        = 0;   // indexer used when converting USART data to u8[]
    u8 last_page                = BOOTLOADER_OFFSET / BOOTLOADER_PAGE_SIZE - 1; // -1 forces 1st time to trigger an erase
    u8 current_addr_page        = 0;   // page # of the current record's first byte
    u8 last_addr_page           = 0;   // page # of the current record's last byte

    MUSART_transmitStr(USART_dev1, "\r\n\r\nSend the HEX file...\r\n");

    while (!isEOF)
    {
        // request 1st half of the record from the UI
        MUSART_transmitChar(USART_dev1, Tx_inform_UI_RecordHalf1);

        // get colon + CC + Addr + RT
        MUSART_receiveData(USART_dev1, rx_buff, 1 + CC_LEN + ADDR_LEN + RT_LEN);

        // convert to u8[]
        for (i = 0; i < (CC_LEN + ADDR_LEN + RT_LEN); i++)
        {
            input[i] = (char)rx_buff[i + 1];
        }

        // store CC
        current_record.bytesCount = LNUM_strHexToU8( &input[0] );

        // store Addr
        current_record.addr = LNUM_strHexToU16( &input[2] );

        // store RT
        current_record.recordType = LNUM_strHexToU8( &input[6]  );

        // request 2nd half of the record from the UI
        MUSART_transmitChar(USART_dev1, Tx_inform_UI_RecordHalf2);

        // get data + checksum
        MUSART_receiveData(USART_dev1, rx_buff, current_record.bytesCount * 2 + CHECKSUM_LEN);

        // convert to u8[]
        for (i = 0; i < (current_record.bytesCount * 2 + CHECKSUM_LEN); i++)
        {
            input[i] = (char)rx_buff[i];
        }

        // store data
        for (i = 0; i < current_record.bytesCount; i++)
        {
            current_record.data[i] = LNUM_strHexToU8(&input[i*2]);
        }

        // store checksum
        current_record.checksum = LNUM_strHexToU8(&input[i*2]);

        // take an action
        switch (current_record.recordType)
        {
            case Record_type_data:
                current_addr = base_addr + current_record.addr;

                // if writing to the bootloader area, then, just skip
                // TODO: throw an error an inform the UI to terminate
                if (current_addr < (FPEC_page_0 + BOOTLOADER_OFFSET))
                {
                    continue;
                }

                last_addr = current_addr + current_record.bytesCount - 1;

                // if odd count, we have to write an arbitrary value along with the last byte,
                // hence an extra byte/address is needed
                if (current_record.bytesCount % 2)
                {
                    last_addr += 1;
                }

                // how to calc page no. :
                // ((addr - 0x08000000) / 0x400) = ((addr - 0x08000000) / 1024) = page no.
                current_addr_page = (current_addr - FPEC_page_0) / BOOTLOADER_PAGE_SIZE;
                last_addr_page    = (last_addr - FPEC_page_0) / BOOTLOADER_PAGE_SIZE;

                // if the first byte we're writing is in a new page
                if (current_addr_page > last_page)
                {
                    last_page = current_addr_page; // save as last page for next rounds
                    MFPEC_flashPageErase(current_addr); // erase this page
                }

                // if the last byte we're writing is in a new page
                if (last_addr_page > last_page)
                {
                    last_page = last_addr_page; // save as last page for next rounds
                    MFPEC_flashPageErase(last_addr); // erase this page
                }

                // store to flash
                for (i = 0; i < current_record.bytesCount / 2; i++) // /2 because we write 2 bytes at a time
                {
                    MFPEC_flashProgram((u16*)current_addr + i, ((const u16*)current_record.data)[i]);
                }

                // if odd count, then the previous loop missed the last byte, we have to write
                // the last byte concatenated with 0xFF (any arbitrary value)
                // writing the last 2 bytes together results in a hard fault, probably because the addresses
                // are not half-word (2-byte) aligned relative to the beginning of the flash
                if (current_record.bytesCount % 2)
                {
                    MFPEC_flashProgram( (u16*)current_addr + i, 0xFF00 | current_record.data[i*2] );
                }
            break;

            case Record_type_EOF:
                isEOF = 1;
            break;

            case Record_type_base:
                /*
                  source:http://www.keil.com/support/docs/1584/
                  the absolute-memory address of a data record is obtained by adding the address
                  field in the record to the shifted base address.
                  ex: Address from the data record's address field      2462
                      base address record data field                FFFF
                                                                    --------
                      Absolute-memory address                       FFFF2462
               */

                // concatenate bytes (reverse endian-ness)
                base_addr = ((u32)current_record.data[0] << 24) + ((u32)current_record.data[1] << 16);
            break;

            case Record_type_last_addr:
                // the data field of this record contains the address of '_start()'
                // we won't catch that because we call the 'Reset_Handler()', which in turn calls '_start()'
            break;

            default:
                // ERROR
            break;
        } // switch (current_record.recordType)
    } // while (!isEOF)

    // store the addr of 'Reset_Handler()' in addr_to_call
    addr_to_call = *(Function_t*)(FPEC_page_0 + BOOTLOADER_OFFSET + 4);
}

void APP_modeEraseFlash(void)
{
    // inc by 1024 each iteration, since each page is 1024 bytes
    for (FPEC_page_t p = FPEC_page_0 + BOOTLOADER_OFFSET; p < FPEC_page_64; p += BOOTLOADER_PAGE_SIZE)
    {
        MFPEC_flashPageErase(p);
    }
}

void APP_modeRunLastSW(void)
{
    // store the addr of 'Reset_Handler()' in addr_to_call
    addr_to_call = *(Function_t*)(FPEC_page_0 + BOOTLOADER_OFFSET + 4);
}

